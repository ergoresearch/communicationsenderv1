﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.MessageSender
{
    public interface ITransportSender
    {
        RequestResult Send(string from, string fromname, string to, string cc, string subject, string body, string phone, Dictionary<String, byte[]> attachements = null);
    }
}
