﻿using CommunicationSender.Enums;
using CommunicationSender.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.MessageSender.Transport
{
    public class EmailTransportSender : ITransportSender
    {
        private readonly SmtpClient _client;
        public EmailTransportSender()
        {
            _client = new SmtpClient(AppConfigData.SMTPMailServer, AppConfigData.SMTPMailServerPort)
            {
                Credentials = new NetworkCredential(AppConfigData.NetworkCredentialEmail, AppConfigData.NetworkCredentialPwd),
                EnableSsl = true
            };
        }

        public RequestResult SendEmail(MailMessage mailMessage)
        {
            if (mailMessage.To == null) return new RequestResult(RequestResultType.Failed, "Invalid email address");

            foreach (var to in mailMessage.To)
            {
                if (string.IsNullOrEmpty(to.Address)) return new RequestResult(RequestResultType.Failed, "Invalid email address");
            }

            try
            {
                _client.Send(mailMessage);
            }
            catch (SmtpException ex)
            {
                return new RequestResult(RequestResultType.Failed, ex.Message);
            }

            return new RequestResult(RequestResultType.Succeeded);
        }

        public RequestResult Send(string from, string fromname, string to, string cc, string subject, string body, string phone, Dictionary<String, byte[]> attachements = null)
        {
            MailMessage message = new MailMessage();

            if (String.IsNullOrEmpty(to))
            {
                return new RequestResult(RequestResultType.Failed, "Adresse courriel invalide");
            }

            message.From = new MailAddress(from, fromname);
            message.To.Add(new MailAddress(to));
            message.CC.Add(new MailAddress(cc));

            if (attachements != null)
            {
                foreach (KeyValuePair<String, byte[]> fileStream in attachements)
                {
                    Attachment attachment = new Attachment(new MemoryStream(fileStream.Value), fileStream.Key);
                    message.Attachments.Add(attachment);
                }
            }

            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            message.Subject = subject;
            message.Body = body;

            return SendEmail(message);
        }
    }
}
