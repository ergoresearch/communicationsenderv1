﻿using CommunicationSender.Enums;
using CommunicationSender.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace CommunicationSender.MessageSender.Transport
{
    public class SMSTransportSender : ITransportSender
    {
        public SMSTransportSender()
        {
            TwilioClient.Init(AppConfigData.TwilioAccountSid, AppConfigData.TwilioAuthToken);
        }


        public RequestResult Send(string from, string fromname, string to, string cc, string subject, string body, string phone, Dictionary<string, byte[]> attachements = null)
        {
            String messageid = "";
            string log = "";

            phone = Formatter.PhoneFormatter(phone);
            
            try
            {

                var message = MessageResource.Create(
                        body: body,
                        from: new Twilio.Types.PhoneNumber(AppConfigData.TwilioPhoneServer),
                        to: new Twilio.Types.PhoneNumber(phone)
                    );
                if (String.IsNullOrEmpty(message.Sid))
                {
                    messageid = message.Sid;
                    log += message.ErrorMessage + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                log += ex.Message + Environment.NewLine;
                return new RequestResult(RequestResultType.Failed, ex.Message);
            }
            return new RequestResult(RequestResultType.Succeeded, messageid);
        }
    }
}
