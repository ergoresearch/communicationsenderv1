﻿using CommunicationSender.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.MessageSender
{
    public class RequestResult
    {
        public RequestResultType ResultType { get; set; }
        public String Message { get; set; }

        public RequestResult(RequestResultType type = RequestResultType.Succeeded, String message = "")
        {
            this.ResultType = type;
            this.Message = message;
        }
    }
}
