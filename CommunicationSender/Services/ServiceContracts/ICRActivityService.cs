﻿using CommunicationSender.DTO;
using CommunicationSender.Enums;
using CommunicationSender.MessageSender;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Services.ServiceContracts
{
    public interface ICRActivityService
    {
        RequestResult SendLateNotice(LateConfirmation lateAppointment, CommunicationConfirmationType sType);
        List<LateConfirmation> GetLateAppointmentsByOwner();
        void CreateEPActivity(String activityType, Guid taskID, String sendto, String messageid, String body, int baccountid, CommunicationConfirmationType ccType);
    }
}
