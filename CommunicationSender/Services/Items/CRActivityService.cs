﻿using CommunicationSender.Constants;
using CommunicationSender.DTO;
using CommunicationSender.Enums;
using CommunicationSender.Helpers;
using CommunicationSender.MessageSender;
using CommunicationSender.MessageSender.Transport;
using CommunicationSender.Repositories.Items;
using CommunicationSender.Repositories.RepoContracts;
using CommunicationSender.Resources;
using CommunicationSender.Services.ServiceContracts;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Services
{
    public class CRActivityService : ICRActivityService
    {
        readonly ICRActivitRepository _repoAct;
        readonly IERChangeDatesRepository _erChangeDates;
        ITransportSender _transportSender;
        RequestResult rResult;
        public CRActivityService()
        {
            _repoAct = new CRActivityRepository();
            _erChangeDates = new ERChangeDatesRepository();
        }

        public List<LateConfirmation> GetLateAppointmentsByOwner()
        {
            var lstOfTodayRdv = _repoAct.FetchTodayActivities();           
            lstOfTodayRdv.ForEach(w => w.StartDate = 
            _erChangeDates.FetchERChangeDate((DateTime)w.StartDate)); //Update startdate due to timezone server difference
            
            var lstOfLateTodayRdv = lstOfTodayRdv.Where(x => 
                                                (x.StartDate.TimeOfDay.Ticks <= DateTime.Now.TimeOfDay.Ticks) &&
                                                (((int)DateTime.Now.Subtract(x.StartDate).TotalMinutes) >= AppConfigData.PeriodCheckInMin))
                                         .ToList();
            return lstOfLateTodayRdv;
        }

        public void CreateEPActivity(String activityType, Guid taskID, String sendto, String messageid, String body, int baccountid, CommunicationConfirmationType ccType)
        {
            //Get info of activity
            var newEventTask = _repoAct.FetchCRActivity(taskID);
            var erchangedateTask = _erChangeDates.FetchERChangeDate(DateTime.Now);
            var newEventErChangedate = _erChangeDates.FetchERChangeDate((DateTime)newEventTask.StartDate);
            string subject = strings.RdvDate.Replace(StringConstants.RDVDATE, newEventErChangedate.ToString());
            body = strings.MsgConfirmation.Replace(StringConstants.SENDTO, sendto)
                                          .Replace(StringConstants.ERCHANGEDATE, erchangedateTask.ToString())
                                          .Replace(StringConstants.BODY, body);
            _repoAct.AddCRActivity(newEventTask, activityType, sendto, subject, body, baccountid, ccType, taskID);
        }

        public RequestResult SendLateNotice(LateConfirmation lateAppointment, CommunicationConfirmationType sType)
        {
            string smsMessageRetard = strings.AvisRetardSMS.Replace(StringConstants.FIRSTNAME, lateAppointment.FormalPatientName)
                                                    .Replace(StringConstants.CLINICIANNAME, lateAppointment.ClinicianName)
                                                    .Replace(StringConstants.TITRE, string.Empty)
                                                    .Replace(StringConstants.DATERDV, lateAppointment.StartDate.ToString("dddd, dd MMMM yyyy", new CultureInfo("fr-CA")))
                                                    .Replace(StringConstants.HEURERDV, lateAppointment.StartDate.ToString("h:mm tt"))
                                                    .Replace(StringConstants.MENTIONLEGAL, strings.MentionLegaleSMS);
            string emailMessageRetard = strings.AvisRetardCourriel.Replace(StringConstants.FIRSTNAME, lateAppointment.FormalPatientName)
                                               .Replace(StringConstants.CLINICIANNAME, lateAppointment.ClinicianName)
                                               .Replace(StringConstants.TITRE, string.Empty)
                                               .Replace(StringConstants.DATERDV, lateAppointment.StartDate.ToString("dddd, dd MMMM yyyy", new CultureInfo("fr-CA")))
                                               .Replace(StringConstants.HEURERDV, lateAppointment.StartDate.ToString("h:mm tt"))
                                               .Replace(StringConstants.MENTIONLEGAL, strings.MentionLegaleCourriel);

            switch (sType)
            {
                case CommunicationConfirmationType.SMS:

                    if (!lateAppointment.AvisRetardSMSDateSent.HasValue)
                    {
                        _transportSender = new SMSTransportSender();
                        rResult = _transportSender.Send(null, null, null, null, strings.AvisRetardSujet, smsMessageRetard, lateAppointment.Phone);


                        CreateEPActivity(CRActivityConstants.SMSAVISRETARDSENT, lateAppointment.NoteID, lateAppointment.Phone,
                                               rResult.Message, smsMessageRetard, lateAppointment.BAccountID,
                                               CommunicationConfirmationType.SMS);
                    }
                    break;
                case CommunicationConfirmationType.EMAIL:

                    if (!lateAppointment.AvisRetardEmailDateSent.HasValue)
                    {
                        _transportSender = new EmailTransportSender();
                        rResult = _transportSender.Send(AppConfigData.FromEmailEquilibre, AppConfigData.TitleEmailEquilibre,
                                                              lateAppointment.Email, lateAppointment.ClinicianEmail, strings.AvisRetardSujet,
                                                              emailMessageRetard, lateAppointment.Phone);

                        CreateEPActivity(CRActivityConstants.EMAILAVISRETARDSENT, lateAppointment.NoteID, lateAppointment.Email, "",
                                               emailMessageRetard, lateAppointment.BAccountID,
                                               CommunicationConfirmationType.EMAIL);
                    }

                    break;
            }

            return rResult;
        }
    }
}
