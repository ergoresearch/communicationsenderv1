﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Helpers
{
    public class AppConfigData
    {
        private const string PERIODCHECKINMIN = "PeriodCheckInMin";
        private const string SMTPMAILSERVER = "SMTPMailServer";
        private const string SMTPMAILSERVERPORT = "SMTPMailServerPort";
        private const string NETWORKCREDENTIALEMAIL = "NetworkCredentialEmail";
        private const string NETWORKCREDENTIALPWD = "NetworkCredentialPwd";
        private const string TWILIOACCOUNTSID = "TwilioAccountSid";
        private const string TWILIOAUTHTOKEN = "TwilioAuthToken";
        private const string FROMEMAILEQUILIBRE = "FromEmailEquilibre";
        private const string TITLEEMAILEQUILIBRE = "TitleEmailEquilibre";
        private const string TWILIOPHONESERVER = "TwilioPhoneServer";

        public static int PeriodCheckInMin => Int32.Parse(ConfigurationManager.AppSettings[PERIODCHECKINMIN] ?? "0");
        public static string SMTPMailServer => ConfigurationManager.AppSettings[SMTPMAILSERVER] ?? "";
        public static int SMTPMailServerPort => Int32.Parse(ConfigurationManager.AppSettings[SMTPMAILSERVERPORT] ?? "0");
        public static string NetworkCredentialEmail => ConfigurationManager.AppSettings[NETWORKCREDENTIALEMAIL] ?? "";
        public static string NetworkCredentialPwd => ConfigurationManager.AppSettings[NETWORKCREDENTIALPWD] ?? "";
        public static string TwilioAccountSid => ConfigurationManager.AppSettings[TWILIOACCOUNTSID] ?? "";
        public static string TwilioAuthToken => ConfigurationManager.AppSettings[TWILIOAUTHTOKEN] ?? "";
        public static string FromEmailEquilibre => ConfigurationManager.AppSettings[FROMEMAILEQUILIBRE] ?? "";
        public static string TitleEmailEquilibre => ConfigurationManager.AppSettings[TITLEEMAILEQUILIBRE] ?? "";
        public static string TwilioPhoneServer => ConfigurationManager.AppSettings[TWILIOPHONESERVER] ?? "";
    }
}
