﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Helpers
{
    public class Formatter
    {
        public static string PhoneFormatter(string phoneNumber)
        {
            phoneNumber = phoneNumber.Replace("-", "").Replace("/", "").Replace(" ", "");
            phoneNumber = "+1" + phoneNumber;
            return phoneNumber;
        }
    }
}
