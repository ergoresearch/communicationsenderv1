﻿using CommunicationSender.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.DTO
{
    public class LateConfirmation
    {
        public string PatientName { get; set; }
        public string FormalPatientName { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime StartDate { get; set; }
        public CommunicationConfirmationType ConfirmationType { get; set; }
        public string ClinicianName { get; set; }
        public string ClinicianEmail { get; set; }
        public CommunicationType AvailType { get; set; }
        public string LinkURL { get; set; }
        public Guid NoteID { get; set; }
        public int BAccountID{ get; set; }
        public DateTime? AvisRetardSMSDateSent { get; set; }
        public DateTime? AvisRetardEmailDateSent { get; set; }
    }
}
