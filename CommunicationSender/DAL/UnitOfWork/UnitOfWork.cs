﻿using CommunicationSender.DAL.Contracts;
using CommunicationSender.Repositories.Items;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AcumaticaDBContext _dbcontext;

        public UnitOfWork()
        {
            _dbcontext = new AcumaticaDBContext();
        }

        public DbContext Context => _dbcontext;

        public DbSet<TEntity> GetRepository<TEntity>()  where TEntity : class => _dbcontext.Set<TEntity>();

        public void Commit() => _dbcontext.SaveChanges();

    }
}
