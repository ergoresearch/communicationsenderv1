﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.DAL.Contracts
{
    public interface IUnitOfWork
    {
        DbContext Context { get; }
        DbSet<TEntity> GetRepository<TEntity>() where TEntity : class;
        void Commit();
    }
}
