﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.DAL.Contracts
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAllBy(Expression<Func<TEntity, bool>> predicate);
        IQueryable<TEntity> GetAllQuery();
        TEntity FindBy(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity entity);
        //void Delete(TEntity entity);
        void Update(TEntity entity);
    }
}
