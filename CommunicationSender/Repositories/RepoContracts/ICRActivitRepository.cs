﻿using CommunicationSender.DTO;
using CommunicationSender.Enums;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Repositories.RepoContracts
{
    public interface ICRActivitRepository
    {
        List<LateConfirmation> FetchTodayActivities();
        CRActivity FetchCRActivity(Guid id);
        void AddCRActivity(CRActivity newEvent, string activityType, string sendto, string subject, string body, int baccountid, CommunicationConfirmationType ccType, Guid taskID);
    }
}
