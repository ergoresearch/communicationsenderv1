﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Repositories.RepoContracts
{
    public interface IERChangeDatesRepository
    {
        DateTime FetchERChangeDate(DateTime dateTime);
    }
}
