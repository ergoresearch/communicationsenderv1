﻿using CommunicationSender.DAL.Contracts;
using CommunicationSender.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Repositories.Items
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private IUnitOfWork _unitOfWork;
        protected DbSet<TEntity> _dbSet;

        public Repository(IUnitOfWork uow)
        {
            _unitOfWork = uow;
            _dbSet = _unitOfWork.GetRepository<TEntity>();
        }

        public IQueryable<TEntity> GetAllQuery() => _dbSet;

        public IEnumerable<TEntity> GetAll() => _dbSet.ToList();

        public IEnumerable<TEntity> GetAllBy(Expression<Func<TEntity, bool>> predicate) => 
               _dbSet.Where(predicate).ToList();

        public TEntity FindBy(Expression<Func<TEntity, bool>> predicate) => _dbSet.FirstOrDefault(predicate);

        public void Add(TEntity entity) => _dbSet.Add(entity);

        public void Update(TEntity entity)
        {
            _dbSet.Attach(entity);
            _unitOfWork.Context.Entry(entity).State = EntityState.Modified;
        }
    }
}
