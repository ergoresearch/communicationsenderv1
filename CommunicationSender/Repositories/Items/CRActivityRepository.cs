﻿using CommunicationSender.Constants;
using CommunicationSender.DAL.Contracts;
using CommunicationSender.DAL.UnitOfWork;
using CommunicationSender.DTO;
using CommunicationSender.Enums;
using CommunicationSender.Helpers;
using CommunicationSender.Repositories.RepoContracts;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Repositories.Items
{
    public class CRActivityRepository : ICRActivitRepository
    {
        readonly IUnitOfWork _uow = new UnitOfWork();
        public readonly IRepository<CRActivity> _cractiviyRepo;
        public readonly IRepository<BAccount> _baccountRepo;
        public readonly IRepository<Contact> _contactRepo;
        public readonly IRepository<User> _usersRepo;
        public readonly IRepository<ERPatientInfo> _erPatientInfoRepo;
        const string ADMIN = CRActivityConstants.ADMIN;
        const int COMPANY_ID = CRActivityConstants.COMPANYID;
        const string NEW_GUID = CRActivityConstants.NEW_GUID;

        public CRActivityRepository()
        {
            _cractiviyRepo = new Repository<CRActivity>(_uow);
            _usersRepo = new Repository<User>(_uow);
            _baccountRepo = new Repository<BAccount>(_uow);
            _contactRepo = new Repository<Contact>(_uow);
            _erPatientInfoRepo = new Repository<ERPatientInfo>(_uow);
        }

        public CRActivity FetchCRActivity(Guid id)
        {
            var cractivity = _cractiviyRepo.FindBy(w => w.NoteID == id);
            return cractivity;
        }

        public void AddCRActivity(CRActivity newEvent, string activityType, string sendto, string subject, string body, int baccountid, CommunicationConfirmationType ccType, Guid taskID)
        {
            string log = string.Empty;
            try
            {
                Guid noteid = Guid.NewGuid();
                var AdminInfo = _usersRepo.FindBy(w => w.Username == ADMIN && w.CompanyID == COMPANY_ID);
                var pkid = AdminInfo == null ? new Guid(NEW_GUID) : AdminInfo.PKID;
                CRActivity cRActivity = _cractiviyRepo.FindBy(w => w.NoteID == taskID);

                CRActivity newEntity;
                newEntity = new CRActivity
                {

                    Subject = subject,
                    BaccountID = baccountid,
                    StartDate = DateTime.Now.ToUniversalTime(),
                    EndDate = DateTime.Now.ToUniversalTime(),
                    OwnerID = pkid,
                    Location = newEvent.Location,
                    UsrBranchCD = newEvent.UsrBranchCD,
                    UsrSubType = newEvent.UsrSubType,
                    UsrNote = sendto,
                    UsrState = newEvent.UsrState,
                    IsPrivate = newEvent.IsPrivate,
                    IsExternal = newEvent.IsExternal,
                    Type = (activityType + "     ").Substring(0, 5),
                    RefNoteID = newEvent.RefNoteID,
                    CreatedByID = pkid,
                    LastModifiedByID = pkid,
                    UsrIsConfirmed = newEvent.UsrIsConfirmed,
                    UsrNewCase = newEvent.UsrNewCase,
                    UsrItemReady = newEvent.UsrItemReady,
                    UsrOTG = newEvent.UsrOTG,
                    NoteID = noteid,
                    CompanyID = 5,
                    //StartTimezone = StartTimezone,
                    CreatedByScreenID = CRActivityConstants.CONFIRM,
                    CreatedDateTime = DateTime.Now.ToUniversalTime(),
                    LastModifiedByScreenID = CRActivityConstants.CONFIRM,
                    LastModifiedDateTime = DateTime.Now.ToUniversalTime(),
                    Synchronize = true,
                    Body = body,
                    Priority = newEvent.Priority,
                    PercentCompletion = 100,
                    UIStatus = CRActivityConstants.OP,
                    ClassID = 2,
                    AllDay = false,

                };
                _cractiviyRepo.Add(newEntity);

                if(ccType == CommunicationConfirmationType.SMS) cRActivity.UsrAvisRetardSMSDateSent = DateTime.Now;
                if(ccType == CommunicationConfirmationType.EMAIL) cRActivity.UsrAvisRetardEmailDateSent = DateTime.Now;
                _cractiviyRepo.Update(cRActivity);

                _uow.Commit();

            }
            catch (Exception ex)
            {
                log += ex.Message + Environment.NewLine;
            }

        }

        public List<LateConfirmation> FetchTodayActivities()
        {
            var actRepo = _cractiviyRepo.GetAllQuery();
            var baccRepo = _baccountRepo.GetAllQuery();
            var contactRepo = _contactRepo.GetAllQuery();
            var userRepo = _usersRepo.GetAllQuery();
            var erpRepo = _erPatientInfoRepo.GetAllQuery();

            List<int> teleconsultation = new List<int> { 3313, 3315, 3316, 3317, 3318, 3319, 3311, 3312, 3326, 3325, 3327, 3328, 3329, 3332, 3337, 3338, 3342 };
            var craLateActivities =  (from act in actRepo
                                      join bacc in baccRepo on act.RefNoteID equals bacc.NoteID
                                      join cont in contactRepo on bacc.DefContactID equals cont.ContactID
                                      join user in userRepo on act.OwnerID equals user.PKID
                                      join patientinfo in erpRepo on bacc.BAccountID equals patientinfo.BAccountID
                                      where (cont.UsrConfirmationType == (int)CommunicationConfirmationType.SMS ||
                                             cont.UsrConfirmationType == (int)CommunicationConfirmationType.EMAIL ||
                                             cont.UsrConfirmationType == (int)CommunicationConfirmationType.BOTH) &&
                                             act.Type == CRActivityTypeConstants.RENDEZVOUS &&
                                             (teleconsultation.Contains(act.UsrSubType.HasValue ? act.UsrSubType.Value : 0)) && 
                                             !act.DeletedDatabaseRecord &&
                                             (act.UsrAvailType == (int)CommunicationType.VIDEO || act.UsrAvailType == (int)CommunicationType.TELEPHONE) &&
                                             (act.StartDate != null && DbFunctions.TruncateTime(act.StartDate.Value) == DateTime.Today) &&
                                             (act.UsrState != (int)UsrState.CLIENT_ARRIVE && act.UsrState != (int)UsrState.RDV_COMPLET &&
                                             act.UsrState != (int)UsrState.NON_PRESENT &&
                                             act.UsrState != (int)UsrState.ANNULE && act.UsrState != (int)UsrState.INCOMPLET)
                                      select new LateConfirmation()
                                      {
                                          PatientName = cont.FirstName + " " + cont.LastName,
                                          FormalPatientName = patientinfo.Gender == null ? "M./Mme " + cont.LastName : patientinfo.Gender == "M" ? "M. " + cont.LastName : patientinfo.Gender == "F" ? "Mme " + cont.LastName : "M./Mme " + cont.LastName,
                                          FirstName = cont.FirstName,
                                          Phone = cont.Phone1,
                                          Email = cont.EMail,
                                          ConfirmationType = cont.UsrConfirmationType == null ? CommunicationConfirmationType.NONE : (CommunicationConfirmationType)cont.UsrConfirmationType,
                                          StartDate = act.StartDate.Value,
                                          ClinicianName = user.FirstName + " " + user.LastName,
                                          ClinicianEmail = user.Email,
                                          AvailType = act.UsrAvailType == null ? CommunicationType.NONE : (CommunicationType)act.UsrAvailType,
                                          LinkURL = act.UsrCalendarEventID,
                                          BAccountID = bacc.BAccountID,
                                          NoteID = act.NoteID,
                                          AvisRetardEmailDateSent = act.UsrAvisRetardEmailDateSent,
                                          AvisRetardSMSDateSent = act.UsrAvisRetardSMSDateSent
                                       }).ToList();

            return craLateActivities;
        }
    }
}
