﻿using CommunicationSender.DAL.Contracts;
using CommunicationSender.DAL.UnitOfWork;
using CommunicationSender.Repositories.RepoContracts;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Repositories.Items
{
    public class ERChangeDatesRepository : IERChangeDatesRepository
    {
        readonly IUnitOfWork _uow = new UnitOfWork();
        public readonly IRepository<ERChangeDate> _erChangesDatesRepo;
        public ERChangeDatesRepository()
        {
            _erChangesDatesRepo = new Repository<ERChangeDate>(_uow);
        }

        public DateTime FetchERChangeDate(DateTime dateTime)
        {
            int? offset = -5;
            var id1 =  _erChangesDatesRepo.FindBy(w => dateTime >= w.FromDate && dateTime <= w.ToDate);
            if (id1 != null) offset = id1.OffSet;
            DateTime newDateTime = TimeZoneInfo.Local.IsDaylightSavingTime(dateTime) ? dateTime.AddHours((double)offset) : dateTime.AddHours((double)offset);
            return newDateTime;
        }
    }
}
