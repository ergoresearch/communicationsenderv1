﻿namespace CommunicationSender.Enums
{
    public enum CommunicationType
    {
        TELEPHONE = 3,
        VIDEO = 4,
        NONE = 5
    }
}
