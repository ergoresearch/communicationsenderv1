﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Enums
{
    public enum RequestResultType
    {
        Succeeded,
        Failed
    }
}
