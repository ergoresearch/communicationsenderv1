﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Enums
{
    public enum CommunicationConfirmationType
    {
        SMS = 0,
        EMAIL = 1,
        BOTH = 3,
        NONE = 4
    }
}
