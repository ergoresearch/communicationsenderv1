﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender.Enums
{
    public enum UsrState
    {
        ANNULE = 2,
        CLIENT_ARRIVE = 3,
        NON_PRESENT = 4,
        RDV_COMPLET = 6,
        INCOMPLET = 7
    }
}
