﻿using CommunicationSender.Constants;
using CommunicationSender.DTO;
using CommunicationSender.Enums;
using CommunicationSender.Helpers;
using CommunicationSender.MessageSender;
using CommunicationSender.MessageSender.Transport;
using CommunicationSender.Resources;
using CommunicationSender.Services;
using CommunicationSender.Services.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSender
{
    class Program
    {
        static void Main(string[] args)
        {
            RequestResult rResult;
            ICRActivityService _cractivityService = new CRActivityService();

            var lateAppointments = _cractivityService.GetLateAppointmentsByOwner();

            foreach (var lateAppointment in lateAppointments)
            {
                switch (lateAppointment.ConfirmationType)
                {
                    //case CommunicationConfirmationType.SMS:
                    //    rResult = _cractivityService.SendLateNotice(lateAppointment, CommunicationConfirmationType.SMS);
                    //    break;
                    case CommunicationConfirmationType.EMAIL:
                        rResult = _cractivityService.SendLateNotice(lateAppointment, CommunicationConfirmationType.EMAIL);
                        break;
                    case CommunicationConfirmationType.SMS:
                    case CommunicationConfirmationType.BOTH:
                        rResult = _cractivityService.SendLateNotice(lateAppointment, CommunicationConfirmationType.SMS);
                        rResult = _cractivityService.SendLateNotice(lateAppointment, CommunicationConfirmationType.EMAIL);
                        break;
                }
            }
        }
    }
}
