namespace Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BAccount")]
    public partial class BAccount
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CompanyID { get; set; }

        [Key]
        [Column(Order = 1)]
        public int BAccountID { get; set; }

        [Required]
        [StringLength(30)]
        public string AcctCD { get; set; }

        [StringLength(60)]
        public string AcctName { get; set; }

        [StringLength(50)]
        public string AcctReferenceNbr { get; set; }

        public int? ParentBAccountID { get; set; }

        public bool ConsolidateToParent { get; set; }

        [Required]
        [StringLength(2)]
        public string Type { get; set; }

        [Required]
        [StringLength(1)]
        public string Status { get; set; }

        [StringLength(10)]
        public string TaxZoneID { get; set; }

        [StringLength(50)]
        public string TaxRegistrationID { get; set; }

        public int? DefContactID { get; set; }

        public int? DefAddressID { get; set; }

        public int? DefLocationID { get; set; }

        public int? SearchID { get; set; }

        [StringLength(10)]
        public string CampaignSourceID { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] tstamp { get; set; }

        public Guid CreatedByID { get; set; }

        [Required]
        [StringLength(8)]
        public string CreatedByScreenID { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public Guid LastModifiedByID { get; set; }

        [Required]
        [StringLength(8)]
        public string LastModifiedByScreenID { get; set; }

        public DateTime LastModifiedDateTime { get; set; }

        public int? WorkGroupID { get; set; }

        public Guid? OwnerID { get; set; }

        [StringLength(10)]
        public string ClassID { get; set; }

        public int ConsolidatingBAccountID { get; set; }

        public bool DeletedDatabaseRecord { get; set; }

        public Guid? NoteID { get; set; }

        [StringLength(32)]
        public string UsrNubisApiKey { get; set; }

        [StringLength(50)]
        public string UsrLegacyCode { get; set; }

        public bool? UsrStopReminder { get; set; }

        public int? UsrParentBAccountID2 { get; set; }

        public int? UsrParentBAccountID3 { get; set; }

        public int? UsrParentBAccountID4 { get; set; }

        public bool? UsrAgenda { get; set; }
    }
}
