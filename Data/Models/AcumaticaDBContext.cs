namespace Data.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AcumaticaDBContext : DbContext
    {
        public AcumaticaDBContext()
            : base("name=AcumaticaDBContext")
        {
        }

        public virtual DbSet<BAccount> BAccounts { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<CRActivity> CRActivities { get; set; }
        public virtual DbSet<ERPatientInfo> ERPatientInfoes { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<ERChangeDate> ERChangeDates { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BAccount>()
                .Property(e => e.Type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BAccount>()
                .Property(e => e.Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BAccount>()
                .Property(e => e.tstamp)
                .IsFixedLength();

            modelBuilder.Entity<BAccount>()
                .Property(e => e.CreatedByScreenID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BAccount>()
                .Property(e => e.LastModifiedByScreenID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BAccount>()
                .Property(e => e.UsrNubisApiKey)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.ContactType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.FaxType)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Phone1Type)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Phone2Type)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Phone3Type)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.DateOfBirth)
                .HasPrecision(0);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Method)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.tstamp)
                .IsFixedLength();

            modelBuilder.Entity<Contact>()
                .Property(e => e.CreatedByScreenID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.LastModifiedByScreenID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Gender)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.MaritalStatus)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Anniversary)
                .HasPrecision(0);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Source)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Resolution)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.DuplicateStatus)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.UsrLanguage)
                .IsFixedLength();

            modelBuilder.Entity<Contact>()
                .Property(e => e.UsrPhone1Ext)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.UsrPhone2Ext)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.UsrPhone3Ext)
                .IsUnicode(false);

            modelBuilder.Entity<CRActivity>()
                .Property(e => e.Type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CRActivity>()
                .Property(e => e.UIStatus)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CRActivity>()
                .Property(e => e.CreatedByScreenID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CRActivity>()
                .Property(e => e.LastModifiedByScreenID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CRActivity>()
                .Property(e => e.tstamp)
                .IsFixedLength();

            modelBuilder.Entity<CRActivity>()
                .Property(e => e.UsrWebStatus)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ERPatientInfo>()
                .Property(e => e.Height)
                .HasPrecision(6, 2);

            modelBuilder.Entity<ERPatientInfo>()
                .Property(e => e.Weight)
                .HasPrecision(6, 2);

            modelBuilder.Entity<ERPatientInfo>()
                .Property(e => e.ShoeSize)
                .HasPrecision(6, 1);

            modelBuilder.Entity<User>()
                .Property(e => e.ApplicationName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.LastHostName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.GuidForPasswordRecovery)
                .IsUnicode(false);
        }
    }
}
