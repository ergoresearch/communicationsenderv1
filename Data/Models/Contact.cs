namespace Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Contact")]
    public partial class Contact
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CompanyID { get; set; }

        [Key]
        [Column(Order = 1)]
        public int ContactID { get; set; }

        public int? BAccountID { get; set; }

        [StringLength(2)]
        public string ContactType { get; set; }

        [StringLength(255)]
        public string FullName { get; set; }

        [Required]
        [StringLength(255)]
        public string DisplayName { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(255)]
        public string Salutation { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string MidName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(255)]
        public string EMail { get; set; }

        [StringLength(255)]
        public string WebSite { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(3)]
        public string FaxType { get; set; }

        [StringLength(50)]
        public string Phone1 { get; set; }

        [StringLength(3)]
        public string Phone1Type { get; set; }

        [StringLength(50)]
        public string Phone2 { get; set; }

        [StringLength(3)]
        public string Phone2Type { get; set; }

        [StringLength(50)]
        public string Phone3 { get; set; }

        [StringLength(3)]
        public string Phone3Type { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DateOfBirth { get; set; }

        public int? DefAddressID { get; set; }

        public bool IsActive { get; set; }

        public int? WorkgroupID { get; set; }

        public Guid? OwnerID { get; set; }

        public int? RouteID { get; set; }

        public Guid? UserID { get; set; }

        [StringLength(1)]
        public string Method { get; set; }

        [StringLength(10)]
        public string LanguageID { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] tstamp { get; set; }

        public Guid CreatedByID { get; set; }

        [Required]
        [StringLength(8)]
        public string CreatedByScreenID { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public Guid LastModifiedByID { get; set; }

        [Required]
        [StringLength(8)]
        public string LastModifiedByScreenID { get; set; }

        public DateTime LastModifiedDateTime { get; set; }

        public int RevisionID { get; set; }

        public bool DeletedDatabaseRecord { get; set; }

        public bool NoFax { get; set; }

        public bool NoMail { get; set; }

        public bool NoMarketing { get; set; }

        public bool NoCall { get; set; }

        public bool NoEMail { get; set; }

        public bool NoMassMail { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

        [StringLength(1)]
        public string MaritalStatus { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Anniversary { get; set; }

        [StringLength(255)]
        public string Spouse { get; set; }

        public string Img { get; set; }

        [StringLength(1)]
        public string Source { get; set; }

        public int? MajorStatus { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [StringLength(2)]
        public string Resolution { get; set; }

        [StringLength(2)]
        public string DuplicateStatus { get; set; }

        [StringLength(10)]
        public string CampaignID { get; set; }

        public DateTime? QualificationDate { get; set; }

        public DateTime? AssignDate { get; set; }

        [StringLength(10)]
        public string ClassID { get; set; }

        public int? ParentBAccountID { get; set; }

        public bool? IsConvertable { get; set; }

        public DateTime GrammValidationDateTime { get; set; }

        public Guid? ConvertedBy { get; set; }

        [StringLength(40)]
        public string ExtRefNbr { get; set; }

        public bool? Synchronize { get; set; }

        public Guid? NoteID { get; set; }

        [StringLength(2)]
        public string UsrLanguage { get; set; }

        [StringLength(50)]
        public string UsrLegacyCode { get; set; }

        public int? UsrConfirmationType { get; set; }

        [StringLength(10)]
        public string UsrPhone1Ext { get; set; }

        [StringLength(10)]
        public string UsrPhone2Ext { get; set; }

        [StringLength(10)]
        public string UsrPhone3Ext { get; set; }

        public bool? UsrInfoLettre { get; set; }

        public DateTime? UsrInfoLettreAddDate { get; set; }

        public DateTime? UsrInfoLettreRemoveDate { get; set; }

        public int? UsrInfoLettreAddFrom { get; set; }

        [StringLength(50)]
        public string UsrInfoLettreStatus { get; set; }

        [StringLength(50)]
        public string UsrInfoLettreID { get; set; }

        public bool? UsrNoEmail { get; set; }
    }
}
