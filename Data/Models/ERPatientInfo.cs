namespace Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ERPatientInfo")]
    public partial class ERPatientInfo
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CompanyID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BAccountID { get; set; }

        [StringLength(20)]
        public string HealthCardNumber { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

        public DateTime? BirthDay { get; set; }

        public decimal? Height { get; set; }

        public decimal? Weight { get; set; }

        public DateTime? HealthCardExpiration { get; set; }

        public decimal? ShoeSize { get; set; }

        public int? BranchID { get; set; }

        [StringLength(100)]
        public string LegacyCode { get; set; }

        public int? Provenance { get; set; }

        public bool? UsrGonarthrose { get; set; }

        public bool? DossierNumerise { get; set; }
    }
}
