namespace Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CompanyID { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid PKID { get; set; }

        [Required]
        [StringLength(64)]
        public string Username { get; set; }

        [StringLength(512)]
        public string ExtRef { get; set; }

        public int Source { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }

        [StringLength(255)]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        public string FullName { get; set; }

        [Required]
        [StringLength(32)]
        public string ApplicationName { get; set; }

        [StringLength(128)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(255)]
        public string Comment { get; set; }

        public int? LoginTypeID { get; set; }

        [StringLength(512)]
        public string Password { get; set; }

        public bool PasswordChangeable { get; set; }

        public bool PasswordChangeOnNextLogin { get; set; }

        public bool PasswordNeverExpires { get; set; }

        public bool AllowPasswordRecovery { get; set; }

        [StringLength(255)]
        public string PasswordQuestion { get; set; }

        [StringLength(255)]
        public string PasswordAnswer { get; set; }

        public bool IsApproved { get; set; }

        public bool? IsPendingActivation { get; set; }

        public bool IsHidden { get; set; }

        public bool Guest { get; set; }

        public DateTime? LastActivityDate { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public DateTime? LastPasswordChangedDate { get; set; }

        public DateTime? CreationDate { get; set; }

        public bool IsOnLine { get; set; }

        public bool IsAssigned { get; set; }

        public bool OverrideADRoles { get; set; }

        [StringLength(50)]
        public string LastHostName { get; set; }

        public DateTime? LockedOutDate { get; set; }

        public DateTime? LastLockedOutDate { get; set; }

        public int? FailedPasswordAttemptCount { get; set; }

        public DateTime? FailedPasswordAttemptWindowStart { get; set; }

        public int? FailedPasswordAnswerAttemptCount { get; set; }

        public DateTime? FailedPasswordAnswerAttemptWindowStart { get; set; }

        [Required]
        [MaxLength(32)]
        public byte[] GroupMask { get; set; }

        [StringLength(60)]
        public string GuidForPasswordRecovery { get; set; }

        public DateTime? PasswordRecoveryExpirationDate { get; set; }

        public bool DeletedDatabaseRecord { get; set; }

        [Required]
        [MaxLength(32)]
        public byte[] CompanyMask { get; set; }

        public Guid? NoteID { get; set; }

        public int? UsrDefaultBranchID { get; set; }

        public bool? UsrViewDefaultBranchOnly { get; set; }

        public bool? UsrViewVicthom { get; set; }

        public bool? UsrViewEquilibre { get; set; }
    }
}
