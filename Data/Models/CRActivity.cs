namespace Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRActivity")]
    public partial class CRActivity
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CompanyID { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid NoteID { get; set; }

        public Guid? ParentNoteID { get; set; }

        public Guid? RefNoteID { get; set; }

        public int ClassID { get; set; }

        [StringLength(5)]
        public string Type { get; set; }

        [Required]
        [StringLength(255)]
        public string Subject { get; set; }

        [StringLength(255)]
        public string Location { get; set; }

        public string Body { get; set; }

        public int? Priority { get; set; }

        [StringLength(2)]
        public string UIStatus { get; set; }

        public int? CategoryID { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? CompletedDate { get; set; }

        public bool? AllDay { get; set; }

        public int? PercentCompletion { get; set; }

        public Guid? OwnerID { get; set; }

        public int? WorkgroupID { get; set; }

        public bool? IsPrivate { get; set; }

        public bool? IsExternal { get; set; }

        public bool Incoming { get; set; }

        public bool Outgoing { get; set; }

        public bool? Synchronize { get; set; }

        public int? BaccountID { get; set; }

        public int? ContactID { get; set; }

        public int? ShowAsID { get; set; }

        public bool IsLocked { get; set; }

        public bool DeletedDatabaseRecord { get; set; }

        public Guid CreatedByID { get; set; }

        [Required]
        [StringLength(8)]
        public string CreatedByScreenID { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public Guid LastModifiedByID { get; set; }

        [Required]
        [StringLength(8)]
        public string LastModifiedByScreenID { get; set; }

        public DateTime LastModifiedDateTime { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] tstamp { get; set; }

        [StringLength(20)]
        public string UsrBranchCD { get; set; }

        public int? UsrSubType { get; set; }

        [StringLength(500)]
        public string UsrNote { get; set; }

        public int? UsrState { get; set; }

        public bool? UsrIsConfirmed { get; set; }

        public bool? UsrNewCase { get; set; }

        public bool? UsrItemReady { get; set; }

        public bool? UsrOTG { get; set; }

        [StringLength(50)]
        public string UsrLegacyCode { get; set; }

        public int? UsrSatisfactionLevel { get; set; }

        [StringLength(255)]
        public string UsrSatisfactionNote { get; set; }

        public bool? UsrIsVoiceMail { get; set; }

        [StringLength(255)]
        public string UsrCalendarEventID { get; set; }

        public int? UsrConsultaionID { get; set; }

        public bool? UsrMessageSent { get; set; }

        [StringLength(2)]
        public string UsrWebStatus { get; set; }

        [StringLength(15)]
        public string UsrAcctCD { get; set; }

        public int? UsrReferID { get; set; }

        public int? UsrAvailType { get; set; }

        public int? UsrBodyPart { get; set; }

        public bool? UsrOrthese { get; set; }

        [StringLength(50)]
        public string UsrProduct { get; set; }

        [StringLength(50)]
        public string UsrSOOrderNbr { get; set; }

        public int? UsrTaskID { get; set; }

        [StringLength(50)]
        public string UsrTextoSID { get; set; }

        public int? UsrRequestKKGID { get; set; }

        [StringLength(50)]
        public string UsrFaxNumber { get; set; }

        public DateTime? UsrAvisRetardSMSDateSent { get; set; }
        public DateTime? UsrAvisRetardEmailDateSent { get; set; }
    }
}
