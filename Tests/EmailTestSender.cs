﻿using CommunicationSender.Constants;
using CommunicationSender.DTO;
using CommunicationSender.Helpers;
using CommunicationSender.Resources;
using CommunicationSender.Services.ServiceContracts;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class EmailTestSender
    {
        SmtpClient _client;
        TestService serviceTester;
        string email = string.Empty, body = string.Empty;

        [SetUp]
        public void Initialize()
        {
            serviceTester = new TestService();
            _client = new SmtpClient(AppConfigData.SMTPMailServer, AppConfigData.SMTPMailServerPort)
            {
                Credentials = new NetworkCredential(AppConfigData.NetworkCredentialEmail, AppConfigData.NetworkCredentialPwd),
                EnableSsl = true
            };
            email = "plouis-jeune@equilibre.net";
        }

        [Test]
        public void SendEmail()
        {
            // Arrange
            MailMessage mailMessage = new MailMessage();
            LateConfirmation appointment = serviceTester.GetLateAppointmentsByOwner();

            mailMessage.From = new MailAddress(AppConfigData.FromEmailEquilibre, AppConfigData.TitleEmailEquilibre);
            mailMessage.To.Add(new MailAddress(email));
            mailMessage.BodyEncoding = Encoding.UTF8;
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = strings.AvisRetardSujet;
            mailMessage.Body = strings.AvisRetardCourriel.Replace(StringConstants.FIRSTNAME, appointment.FormalPatientName)
                                                         .Replace(StringConstants.CLINICIANNAME, appointment.ClinicianName)
                                                         .Replace(StringConstants.TITRE, string.Empty)
                                                         .Replace(StringConstants.MENTIONLEGAL, strings.MentionLegaleCourriel);

            try
            {
                // Act
                _client.Send(mailMessage);

            }
            catch (SmtpException ex)
            {
                throw new Exception("Error message :" + ex.Message);
            }

        }
    }
}
