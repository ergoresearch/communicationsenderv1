﻿using CommunicationSender.DTO;
using CommunicationSender.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public class TestService
    {
        StagingData sData;
        public TestService()
        {
            sData = new StagingData();
        }

        public LateConfirmation GetLateAppointmentsByOwner()
        {
            var lateAppointments = sData.FetchStagingTodayActivities();
            var lstOfLateTodayRdv = lateAppointments.Where(x =>
                                                (x.StartDate.TimeOfDay.Ticks <= DateTime.Now.TimeOfDay.Ticks) &&
                                                (((int)DateTime.Now.Subtract(x.StartDate).TotalMinutes) > AppConfigData.PeriodCheckInMin))
                                                .Take(1)                         
                                                .ToList();
            return lstOfLateTodayRdv.FirstOrDefault();
        }
    }
}
