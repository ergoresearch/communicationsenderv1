﻿using CommunicationSender.DAL.Contracts;
using CommunicationSender.DAL.UnitOfWork;
using CommunicationSender.DTO;
using CommunicationSender.Enums;
using CommunicationSender.Repositories.Items;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public class StagingData
    {
        readonly IUnitOfWork _uow = new UnitOfWork();
        public readonly IRepository<CRActivity> _cractiviyRepo;
        public readonly IRepository<BAccount> _baccountRepo;
        public readonly IRepository<Contact> _contactRepo;
        public readonly IRepository<User> _usersRepo;
        public readonly IRepository<ERPatientInfo> _erPatientInfoRepo;

        public StagingData()
        {
            _cractiviyRepo = new Repository<CRActivity>(_uow);
            _usersRepo = new Repository<User>(_uow);
            _baccountRepo = new Repository<BAccount>(_uow);
            _contactRepo = new Repository<Contact>(_uow);
            _erPatientInfoRepo = new Repository<ERPatientInfo>(_uow);
        }

        public List<LateConfirmation> FetchStagingTodayActivities()
        {
            var actRepo = _cractiviyRepo.GetAllQuery();
            var baccRepo = _baccountRepo.GetAllQuery();
            var contactRepo = _contactRepo.GetAllQuery();
            var userRepo = _usersRepo.GetAllQuery();
            var erpRepo = _erPatientInfoRepo.GetAllQuery();

            var craLateActivities = (from act in actRepo
                                           join bacc in baccRepo on act.RefNoteID equals bacc.NoteID
                                           join cont in contactRepo on bacc.DefContactID equals cont.ContactID
                                           join user in userRepo on act.OwnerID equals user.PKID
                                           join patientinfo in erpRepo on bacc.BAccountID equals patientinfo.BAccountID
                                           where (cont.UsrConfirmationType == (int)CommunicationConfirmationType.SMS ||
                                                  cont.UsrConfirmationType == (int)CommunicationConfirmationType.EMAIL ||
                                                  cont.UsrConfirmationType == (int)CommunicationConfirmationType.BOTH)
                                           //where act.StartDate != null && DbFunctions.TruncateTime(act.StartDate.Value) == DateTime.Today
                                           orderby act.StartDate descending
                                           select new LateConfirmation()
                                           {
                                               PatientName = cont.FirstName + " " + cont.LastName,
                                               FormalPatientName = patientinfo.Gender == null ? "M./Mme " + cont.LastName : patientinfo.Gender == "M" ? "M. " + cont.LastName : patientinfo.Gender == "F" ? "Mme " + cont.LastName : "M./Mme " + cont.LastName,
                                               FirstName = cont.FirstName,
                                               Phone = cont.Phone1,
                                               Email = cont.EMail,
                                               ConfirmationType = cont.UsrConfirmationType == null ? CommunicationConfirmationType.NONE : (CommunicationConfirmationType)cont.UsrConfirmationType,
                                               StartDate = act.StartDate.Value,
                                               ClinicianName = user.FirstName + " " + user.LastName,
                                               ClinicianEmail = user.Email,
                                               AvailType = act.UsrAvailType == null ? CommunicationType.NONE : (CommunicationType)act.UsrAvailType,
                                               LinkURL = act.UsrCalendarEventID,
                                               BAccountID = bacc.BAccountID,
                                               NoteID = act.NoteID
                                           }).Take(200).ToList();

            return craLateActivities;
        }
    }
}
