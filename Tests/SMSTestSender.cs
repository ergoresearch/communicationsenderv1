﻿using CommunicationSender.Constants;
using CommunicationSender.DAL.Contracts;
using CommunicationSender.DTO;
using CommunicationSender.Helpers;
using CommunicationSender.Resources;
using CommunicationSender.Services;
using CommunicationSender.Services.ServiceContracts;
using Data.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Tests
{
    [TestFixture]
    public class SMSTestSender
    {
        TestService serviceTester;
        string testPhone = string.Empty, body = string.Empty;

        [SetUp]
        public void Initialize()
        {
            TwilioClient.Init(AppConfigData.TwilioAccountSid,
                              AppConfigData.TwilioAuthToken);
            serviceTester = new TestService();
        }

        [Test]
        public void SendSMS()
        {
            // Arrange
            //testPhone = "+15147978183";
            LateConfirmation appointment = serviceTester.GetLateAppointmentsByOwner();
            string body = strings.AvisRetardSMS.Replace(StringConstants.FIRSTNAME, appointment.FormalPatientName)
                                               .Replace(StringConstants.CLINICIANNAME, appointment.ClinicianName)
                                               .Replace(StringConstants.TITRE, string.Empty)
                                               .Replace(StringConstants.MENTIONLEGAL, strings.MentionLegaleSMS);
            try
            {
                // Act
                if (appointment == null) Assert.IsNull(appointment, "no appointment", null);
                testPhone = Formatter.PhoneFormatter(appointment.Phone);

                var message = MessageResource.Create(
                    body: body,
                    from: new Twilio.Types.PhoneNumber(AppConfigData.TwilioPhoneServer),
                    to: new Twilio.Types.PhoneNumber(testPhone)
                );

                // Assert
                Assert.IsTrue(!string.IsNullOrEmpty(message.Sid));
            }
            catch (Exception ex)
            {
                throw new Exception("Error message :" + ex.Message);
            }
        }
    }
}
